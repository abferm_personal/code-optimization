/*
  Blink
 Turns on an LED on for one second, then off for one second, repeatedly.
 
 This example code is in the public domain.
 */

// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;

// Function Prototype
uint8_t increment(uint8_t some_number);

// the setup routine runs once when you press reset:
void setup() {       
  // initialize serial communications at 9600 bps   
  Serial.begin(115200);    
}

// the loop routine runs over and over again forever:
void loop() {
  //we will use this variable to time execution
  unsigned int time = 0;
  uint8_t j = 0;

  //log the time
  time = micros();
  do {

    j = increment(j);

  }
  while (j < 10);  

  time = micros() - time;






  Serial.print("Execution time: ");
  Serial.print(time);
  Serial.print("\r\n");


  while(1);

}

uint8_t increment(uint8_t some_number){
  some_number ++;
  return some_number;
}
