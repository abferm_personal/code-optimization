/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;

// the setup routine runs once when you press reset:
void setup() {       
  // initialize serial communications at 9600 bps   
  Serial.begin(9600);    
}

// the loop routine runs over and over again forever:
void loop() {
  //we will use this variable to time execution
  unsigned int time = 0;
  int j = 10;
  
  //log the time
  time = micros();
    
  //this loop executes a command a given number of times
  
  if(j < 2){
    //get the difference from the time now
    time = micros() - time;
  }else if(j == 2){
    //get the difference from the time now
    time = micros() - time;
  }else if(j == 3){
    //get the difference from the time now
    time = micros() - time;
  }else if(j == 4){
    //get the difference from the time now
    time = micros() - time;
  }else if(j == 5){
    //get the difference from the time now
    time = micros() - time;
  }else if(j == 6){
    //get the difference from the time now
    time = micros() - time;
  }else if(j == 7){
    //get the difference from the time now
    time = micros() - time;
  }else if(j == 8){
    //get the difference from the time now
    time = micros() - time;
  }else if(j == 9){
    //get the difference from the time now
    time = micros() - time;
  }else if(j > 9){
    //get the difference from the time now
    time = micros() - time;
  }

   

    
  Serial.print("Number of checks: ");
  Serial.print(j);
  Serial.print("\n");
  Serial.print("Execution time: ");
  Serial.print(time);
  Serial.print("\n");
  
  
  while(1);
  
}
