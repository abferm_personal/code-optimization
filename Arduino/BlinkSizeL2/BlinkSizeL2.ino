/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;
uint16_t testFunction(){
  return 3;
}

// the setup routine runs once when you press reset:
void setup() {                
  // initialize serial communications at 9600 bps   
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  //we will use this variable to time execution
  unsigned int time = 0;
  uint16_t testVar;
  int j = 100;
  uint8_t loop_cnt = j;
  
  //log the time
  time = micros();
  do {
    PORTB ^= 0x01;
  } while (--loop_cnt);
  
  
  time = micros() - time;
  Serial.print("Number of executions: ");
  Serial.print(j);
  Serial.print("\n");
  Serial.print("Execution time: ");
  Serial.print(time);
  Serial.print("\n");

  while(1);
}
