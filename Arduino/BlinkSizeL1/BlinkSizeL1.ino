/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;

// the setup routine runs once when you press reset:
void setup() {       
  // initialize serial communications at 9600 bps   
  Serial.begin(9600);    
}

// the loop routine runs over and over again forever:
void loop() {
  //we will use this variable to time execution
  unsigned int time = 0;
  int j = 100;
  
  //log the time
  time = micros();
    
  //this loop executes a command a given number of times
  for(int i = 0; i < j; i++){
    PORTB ^= 0x01;
  }
   
  //get the difference from the time now
  time = micros() - time;
    
  Serial.print("Number of executions: ");
  Serial.print(j);
  Serial.print("\n");
  Serial.print("Execution time: ");
  Serial.print(time);
  Serial.print("\n");
  
  
  while(1);
  
}
